<?php
/**
 * Created 26.11.19
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */
?>

<div class="container-fluid">
  <div class="row">
    <div class="col">
      <h2>Metro Publisher To WP Options</h2>
      <!-- form api start -->
      <div class="card">

        <div class="card-body">
          <p>Enter the API key and Secret Key, how to get <a href="https://api.metropublisher.com/quickstart.html">Api
              key?</a></p>
          <form action="" class="form-api">
            <div class="row">
              <div class="col-6">
                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="api-key">Api Key</span>
                  </div>
                  <input type="text" class="form-control" aria-describedby="api-key" name="key"
                    value="<?php echo get_option('mpt_key', false)?>">
                </div>
              </div>
              <div class="col-6">
                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="secret-key">Secret Key</span>
                  </div>
                  <input type="text" class="form-control" aria-describedby="secret-key" name="secret"
                    value="<?php echo get_option('mpt_secretKey', false)?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 d-flex justify-content-end">
                <input type="submit" value="Save" class="btn btn-success">
              </div>
            </div>
          </form>
        </div>

      </div>
      <!-- form api end -->

      <!-- start form post type -->
      <div class="card">
        <div class="card-body">
          <p>Choose the type of posts that will be automatically sent</a></p>
          <form action="" class="form-post-type">
            <div class="row">
              <div class="col-6">
                <div class="form-group">
                  <label for="post-type">Select Post Typ</label>
                  <select multiple class="form-control" id="post-type">
                    <?php
                      $post_type_option = get_option('mpt_post_type', false);
                    ?>
                    <?php foreach (get_post_types(['public'=>true]) as $key => $post_type):?>
                    <option value="<?php echo $key;?>"
                      <?php echo in_array($key, $post_type_option, true)?'selected':''?>>
                      <?php echo $post_type;?>
                    </option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
              <!-- <div class="col-6">
                <div class="form-group">
                  <label for="status_post">Select Post Status</label>
                  <?php
                    $post_status_option = get_option('mpt_post_status', false);
                    $post_status = ['Publish', 'Draft', 'Future', 'Pending', 'Private'];
                  ?>
              <select multiple class="form-control" id="status_post">
                <?php foreach ($post_status as $status):?>
                <option value="<?php echo strtolower($status);?>" <i>
                  </i>
                  <!-- %pcs-comment-start#<?php echo in_array(strtolower($status), $post_status_option, true)?'selected':''?>>
                  <?php echo $status;?>
                </option>
                <?php endforeach;?>
              </select>
            </div>
        </div> -->
            </div>
            <div class="row">
              <div class="col-12">
                <div class="form-check">
                  <!-- <?php $auto = get_option('mpt_auto_publish', false);?>
            <input class="form-check-input" type="checkbox" value="" id="send_new_post" <?php echo $auto == true?'checked':''?>>
            <label class="form-check-label" for="send_new_post">
              Send publications automatically.<span>(If this checkbox is checked, then all post will be
                automatically sent after publication)</span>
            </label> -->
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 d-flex justify-content-end">
                <input type="submit" value="Save" class="btn btn-success">
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- end from post type -->
    </div>
  </div>
</div>
