<?php
/**
 * Plugin Name: Metro Publisher To WP
 * Plugin URI: http://www.justwebagency.com/
 * Description: Creates duplicate posts in Metro Publisher CMS
 * Version: 1.0.0
 * Author: Alex L
 * Author URI: https://gitlab.com/AlsconWeb
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

define('MPT_TYPE', 'mpt_wp');

if (!defined('WPINC')) {
    die;
}

/**
 * The code that runs during plugin activation (but not during updates).
 */
function activate_mpt_wp()
{
    if (version_compare(phpversion(), '5.4', '<')) {
        deactivate_plugins(plugin_basename(__FILE__));
        wp_die('Metro Publisher To WP requires PHP version 5.4 or higher. Plugin was deactivated.');
    }
}

register_activation_hook(__FILE__, 'activate_mpt_wp');
require_once dirname(__FILE__) . '/include/init.php';

add_action('admin_menu', 'mpt_wp_init');

function mpt_wp_init()
{
    add_menu_page('Metro Publisher', 'Metro Publisher', 'remove_users', MPT_TYPE . '-option', 'option_page', plugins_url('/MetroPublisherToWP/assets/img/logo.svg'), '63.1');
}
add_action('init', 'init_auto_publish');
function init_auto_publish()
{
    $key = get_option('mpt_key', false);
    $secret = get_option('mpt_secretKey', false);
    $auto = get_option('mpt_auto_publish', false);
    
    if ($key && $secret && !$key == '' && !$secret == '' && $auto) {
        // $autoSendOn = new MPT_WP($key, $secret);
        // add_action('draft_to_publish', 'auto_send_on');
        // var_dump('Init');
        // $autoSendOn->auto_send();
    }
    $metro = new MPT_WP($key, $secret);
}

function auto_send_on($post)
{
    $key = get_option('mpt_key', false);
    $secret = get_option('mpt_secretKey', false);
    $autoSendOn = new MPT_WP($key, $secret);
    $autoSendOn->auto_send($post, true);
}

function option_page()
{
    require_once dirname(__FILE__) . '/page/option/option.php';
}

add_action('wp_ajax_nopriv_checkApi', 'checkApi_key');
add_action('wp_ajax_checkApi', 'checkApi_key');

function checkApi_key()
{
    $check = new MPT_WP;
    $check->checkApi_key($_POST['key'], $_POST['secret']);
}

add_action('wp_ajax_nopriv_opt_post_type', 'option_post_type');
add_action('wp_ajax_opt_post_type', 'option_post_type');

function option_post_type()
{
    $post_type_option = new MPT_WP;
    $post_type_option->post_active($_POST['post_type'], $_POST['status'], $_POST['auto']);
}

add_action('wp_ajax_nopriv_send_to_api', 'send_post_to');
add_action('wp_ajax_send_to_api', 'send_post_to');
function send_post_to()
{
    $post = $_POST;
    $key = get_option('mpt_key', false);
    $secret = get_option('mpt_secretKey', false);
    
    if (!wp_verify_nonce($_POST['nonce'], 'ajax')) {
        wp_send_json_success(['status'=>'error', 'message'=>'Nonce code no verify']);
    }
    
    $send_post = new MPT_WP($key, $secret);
    $res = $send_post->sendImage($post)->sendNewArticle($post);
}

add_action('add_meta_boxes', 'button_send');
function button_send()
{
    $post_type_active = new MPT_WP;

    add_meta_box('button-send', 'Send to Api Metro Publisher', 'add_button_send', $post_type_active->getPostTypeActive(), 'side', 'high', array(
        '__back_compat_meta_box' => false,
    ));
}

function add_button_send($post, $meta)
{
    wp_nonce_field(plugin_basename(__FILE__), 'MetroToWP');
    if ($post->post_status == 'publish') {
        echo '<p>Publish Article in Metro Publisher</p>';
        echo '<input type="hidden" name="send_to" id="send_to" value="'.$post->ID.'">';
        echo '<button type="button" aria-disabled="false" class="btn-metabox components-button editor-post-publish-button is-button is-default is-primary is-large">Send</button>';
    }
}


add_action('admin_enqueue_scripts', 'mpt_add_script');
function mpt_add_script($hook_suffix)
{
    wp_enqueue_style('mpt-css-admin', plugin_dir_url(__FILE__) . '/assets/css/admin-style.css');
    wp_enqueue_script('sweetalert', '//cdn.jsdelivr.net/npm/sweetalert2@8', array('jquery'), '', true);
    
    if ($hook_suffix == 'toplevel_page_mpt_wp-option') {
        wp_enqueue_style('bootstrap-admin', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');

        wp_enqueue_script('boostrap-js', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '', true);

        wp_enqueue_script('main', plugin_dir_url(__FILE__) . '/assets/js/mpt_main.js', array('jquery'), '', true);

        wp_localize_script('main', 'mpt_data', array(
            'ajaxUrl' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('set_url'),
        ));
    }
    wp_enqueue_script('send_ajax', plugin_dir_url(__FILE__) . '/assets/js/send_ajax.js', array('jquery'), '', true);
    wp_localize_script('send_ajax', 'mpt_data', array(
        'ajaxUrl' => admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('ajax'),
    ));
}
