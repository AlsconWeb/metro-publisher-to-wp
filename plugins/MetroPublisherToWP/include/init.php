<?php

use MetroPublisher\MetroPublisher;
use MetroPublisher\Api\Models\Article;
use MetroPublisher\Api\Models\File;


use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class MPT_WP
{
    public $key;
    public $secretKey;
    public $post_type;
    public $status;
    
    private $auto;
    private $imageID;
    private $postID;
    private $post;

    public function __construct($key='', $secretKey="")
    {
        $this->key = $key;
        $this->secretKey = $secretKey;

        $this->post_type = get_option('mpt_post_type', false);
        $this->status = get_option('mpt_post_status', false);
        $this->auto = null;

        require_once dirname(__FILE__) . '/../vendor/autoload.php';

        $this->addActions();
    }
    
    
    public function init()
    {
        add_option('mpt_key', $this->key, '', 'no');
        add_option('mpt_secretKey', $this->secretKey, '', 'no');
        add_option('mpt_post_type', $this->post_type, '', 'no');
        add_option('mpt_post_status', $this->status, '', 'no');
        add_option('mpt_auto_publish', $this->auto, '', 'on');

        $metroPublisher = new MetroPublisher($this->key, $this->secretKey);
    }

    public function checkApi_key($key, $secretKey)
    {
        $this->key = $key;
        $this->secretKey = $secretKey;

        $check = new MetroPublisher($this->key, $this->secretKey);
        $resp = $check->getAuth();

        if ($resp['status'] == 'true') {
            update_option('mpt_key', $this->key, false);
            update_option('mpt_secretKey', $this->secretKey, false);
            wp_send_json_success(['status'=>'ok', 'key'=>$this->key, 'secret'=>$this->secretKey, 'res'=>$resp]);
        } else {
            wp_send_json_success($resp);
        }
    }

    public function post_active($post, $status, $auto)
    {
        $this->post_type = $post;
        $this->status = $status;
        $this->auto = $auto;

        update_option('mpt_post_type', $this->post_type, false);
        update_option('mpt_post_status', $this->status, false);
        update_option('mpt_auto_publish', $this->auto, false);

        wp_send_json_success(['status'=>'ok']);
    }

    public function getPostTypeActive()
    {
        $new_options_postTyp =[];
        foreach ($this->post_type as $post_type) {
            $new_options_postTyp[]=$post_type;
        }
        return $new_options_postTyp;
    }

    public function sendNewArticle($postID=null)
    {
        if ($this->postID == null) {
            $this->postID = (int) $postID["postID"];
        }
        $this->post = get_post($this->postID);
        
        $rand = rand(0, 100000);

        $uuid3 = Uuid::uuid3(Uuid::NAMESPACE_DNS, 'mpt_wp_aricle-'.$rand);
        $uuid = $uuid3->toString();

        $send = new MetroPublisher($this->key, $this->secretKey);

        $content = get_the_content(null, false, $this->post);
        $content= html_entity_decode(preg_replace("/U\+([0-9A-F]{4})/", "&#x\\1;", $content), ENT_NOQUOTES, 'UTF-8');
        
        $content = strip_tags($content);
    
        $article = new Article($send);
        $article->setUuid($uuid)
            ->setUrlname($this->post->post_name)
            ->setTitle($this->post->post_title)
            ->setMetaTitle($this->post->post_title)
            ->setDescription($this->post->post_excerpt)
            ->setMetaDescription($this->post->post_excerpt)
            ->setPrintDescription($this->post->post_excerpt)
            ->setFeatureImageUuid($this->imageID)
            ->setContent('')
            ->setTeaserImageUuid($this->imageID)
            ->setIssued(new DateTime('now'))
            ->setEvergreen(true);
        $article->save();

        $sendArryGut = [
          "id" => $this->post->ID,
          'date'=> $this->post->post_date,
          'date_gmt'=> $this->post->post_date_gmt,
          'guid'=> [
            'rendered'=>$this->post->guid,
            'raw'=>$this->post->guid
          ],
          'modified'=> $this->post->post_modified,
          'modified_gmt'=> $this->post->post_modified_gmt,
          'password'=> $this->post->post_password,
          'slug'=> $this->post->post_name,
          'status'=> $this->post->post_status,
          'type'=> $this->post->post_type,
          'link'=> $this->post->guid,
          'title'=> [
            'raw'=>$this->post->post_title,
            'rendered'=>$this->post->post_title,
          ],
          'content' => [
            'raw' => $this->post->post_content,
            'rendered' => $content,
            'protected' => false,
            'block_version' => 0,
          ],
          'excerpt' => [
            'raw' => '',
            'rendered' => $this->post->post_excerpt,
            'protected' => false,
          ],
          'author' => $this->post->post_author,
          'featured_media' => 0,
          'comment_status' => 'open',
        ];

        if ($this->post->post_type == 'post') {
            die(json_encode($sendArryGut));
        } else {
            wp_send_json_success(['status'=>'ok', 'uuid' =>$uuid]);
        }
    }


    public function sendImage($postID = null)
    {
        if ($this->postID == null) {
            $this->postID = (int) $postID["postID"];
        }
        $this->post = get_post($this->postID);
        if (has_post_thumbnail($this->postID)) {
            $rand = rand(0, 10000);

            $uuid3 = Uuid::uuid3(Uuid::NAMESPACE_DNS, 'mpt_wp_image'.$rand);
            $uuid = $uuid3->toString();

            $id = get_post_thumbnail_id($this->postID);

            $pathImage = get_attached_file($id);
            $imageMeta = wp_get_attachment_metadata($id);
            
            foreach ($imageMeta["sizes"] as $image) {
                $mimeType = $image["mime-type"];
                $imageName = $image["file"];
                break;
            }


            $authorNamme  = get_user_meta($this->post->post_author, 'nickname', true);

            $metroPublisher = new MetroPublisher($this->key, $this->secretKey);
            $metroPublisher->getHttpClient()->setSslVerification(false);
            $file = new File($metroPublisher);
            $file->setTitle($this->post->post_title)
              ->setUuid($uuid)
              ->setDescription($this->post->post_excerpt)
              ->setFilename($imageName)
              ->setFileType($mimeType)
              ->setCredits($authorNamme)
              ->setFileContent(file_get_contents($pathImage));
    
            $res = $file->save();
            $this->imageID = $res["uuid"];
        } else {
            $this->imageID = null;
        }
        return $this;
    }

    public function auto_send($post, $auto = null)
    {
        $this->postID = $post->ID;
        $this->post = $post;
        $this->auto = $auto;
  
        $this->sendImage()->sendNewArticle();
    }

    public function addActions()
    {
        add_filter('bulk_actions-edit-post', [$this,'mas_send']);
        add_filter('bulk_actions-edit-news', [$this,'mas_send']);
        add_filter('handle_bulk_actions-edit-news', [$this, 'masSendHandler'], 10, 3);
        add_filter('handle_bulk_actions-edit-post', [$this, 'masSendHandler'], 10, 3);
        add_action('admin_notices', [$this,'adminNoticeMass']);
    }
    public function mas_send($bulk_actions)
    {
        $bulk_actions['send_to_metro'] = 'Send To Metro Publisher';
        return $bulk_actions;
    }

    public function masSendHandler($redirect_to, $doaction, $post_ids)
    {
        if ($doaction !== 'send_to_metro') {
            return $redirect_to;
        }

        foreach ($post_ids as $post_id) {
            $this->postID = $post_id;
            $this->post = get_post($post_id);
            $this->sendImage()->sendNewArticle();
        }
        $redirect_to = add_query_arg('send_to_metro', count($post_ids), $redirect_to);
        return $redirect_to;
    }

    public function adminNoticeMass()
    {
        if (! empty($_REQUEST['send_to_metro'])) {
            $ass_count = intval($_REQUEST['send_to_metro']);
            printf('<div id="message" class="updated fade">' .
            _n(
                'Send to %s .',
                'Send to %s .',
                $ass_count,
                'masSendHandler'
            ) . '</div>', $ass_count);
        }
    }
}
