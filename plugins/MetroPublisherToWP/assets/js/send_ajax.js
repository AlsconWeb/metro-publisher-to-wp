jQuery(document).ready(function ($) {
  
  $('.btn-metabox').click(function (e) { 
    e.preventDefault();
      var data = {
        action: 'send_to_api',
        postID: $('#send_to').val(),
        nonce: mpt_data.nonce
      }
      console.log('data', data);

      $.ajax({
        type: "post",
        url: mpt_data.ajaxUrl,
        data: data,
        beforeSend:()=>{
          Swal.showLoading();
        },
        success: function (res) {
          console.log('Res', res);
          Swal.fire({
            position: 'center',
            type: 'success',
            title: 'Article send to Metro Publish',
            showConfirmButton: false,
            timer: 2500
          })
        },
        error: function (error){
          console.log(error.responseText)
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'error',
            html:error.responseText,
            showConfirmButton: false,
            timer: 4500
          })
        }
      });
  });

});