console.log('On Main JS');
jQuery(document).ready(function ($) {
  $('.form-api').submit(function (e) {
    e.preventDefault();

    var data = {
      action: 'checkApi',
      key : $('input[name=key]').val(),
      secret: $('input[name=secret]').val()
    }

    $.ajax({
      type: "post",
      url: mpt_data.ajaxUrl,
      data: data,
      success: function (res) {
        if(res.data.status == 'ok'){
          Swal.fire({
            position: 'center',
            type: 'success',
            title: 'Api key Valid',
            showConfirmButton: false,
            timer: 2500
          })
        }else{
          Swal.fire({
            position: 'center',
            type: 'error',
            title: res.data.message,
            showConfirmButton: false,
            timer: 2500
          })
        }
      }
    });

  });

  $('.form-post-type').submit(function (e) { 
    e.preventDefault();

    var post_type = $('#post-type option');
    var post_status = $('#status_post option');

    var postTypeArr = [];
    var postStatusArr = [];

    $.each(post_type, function (i, el) { 
       if($(this).prop('selected')){
         postTypeArr.push($(this).val())
       }
    });

    $.each(post_status, function (i, el) { 
       if($(this).prop('selected')){
        postStatusArr.push($(this).val())
       }
    });
    
    var data = {
      action:"opt_post_type", 
      post_type:postTypeArr,
      status:postStatusArr,
      auto:$('#send_new_post').prop('checked'),
    }

    console.log('data',data );
    $.ajax({
      type: "post",
      url: mpt_data.ajaxUrl,
      data: data,
      success: function (res) {
        if(res.data.status =="ok"){
          Swal.fire({
            position: 'center',
            type: 'success',
            title: 'Options Save',
            showConfirmButton: false,
            timer: 2500
          })
        }
      }
    });
  });
});

